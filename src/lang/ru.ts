export const ru = {
  header: {
    comics: 'Комиксы',
  },
  comics: {
    entry: {
      search: 'Что бы такого почитать...',
      home: {
        more: 'ещё',
        recentAdditional: 'Новое',
        mostPopular: 'Популярное',
        lastUpdates: 'Недавно обновленные',
      },
      more: {
        more: 'Загрузить ещё',
      },
      demographic: {
        Shounen: '',
        Shoujo: '',
        Seinen: '',
        Josei: '',
      },
      genres: {
        name: 'Жанры',
        list: {
            0: 'арт',
            1: 'боевик',
            2: 'боевые искусства',
            3: 'вампиры',
            4: 'гарем',
            5: 'гендерная интрига',
            6: 'героическое фэнтези',
            7: 'детектив',
            8: 'дзёсэй',
            9: 'додзинси',
          10: 'драма',
          11: 'игра',
          12: 'история',
          13: 'киберпанк',
          14: 'кодомо',
          15: 'комедия',
          16: 'махо-сёдзё',
          17: 'меха',
          18: 'научная фантастика',
          19: 'повседневность',
          20: 'постапокалиптика',
          21: 'приключения',
          22: 'психология',
          23: 'романтика',
          24: 'самурайский боевик',
          25: 'сверхъестественное',
          26: 'сёдзё',
          27: 'сёдзё-ай',
          28: 'сёнэн',
          29: 'сёнэн-ай',
          30: 'спорт',
          31: 'сэйнэн',
          32: 'трагедия',
          33: 'триллер',
          34: 'ужасы',
          35: 'фэнтези',
          36: 'школа',
          37: 'этти',
          38: 'юри',
        },
        // list: [
        // 	/* art */            'арт',
        // 	/* action */         'боевик',
        // 	/* martialArts */    'боевые искусства',
        // 	/* vampires */       'вампиры',
        // 	/* harem */          'гарем',
        // 	/* genderIntriga */  'гендерная интрига',
        // 	/* heroicFantasy */  'героическое фэнтези',
        // 	/* detective */      'детектив',
        // 	/* josei */          'дзёсэй',
        // 	/* doujinshi */      'додзинси',
        // 	/* drama */          'драма',
        // 	/* game */           'игра',
        // 	/* historical */     'история',
        // 	/* cyberpunk */      'киберпанк',
        // 	/* codomo */         'кодомо',
        // 	/* comedy */         'комедия',
        // 	/* mahoShoujo */     'махо-сёдзё',
        // 	/* mecha */          'меха',
        // 	/* sciFi */          'научная фантастика',
        // 	/* natural */        'повседневность',
        // 	/* postapocalypse */ 'постапокалиптика',
        // 	/* adventure */      'приключения',
        // 	/* psychological */  'психология',
        // 	/* romance */        'романтика',
        // 	/* samurai */        'самурайский боевик',
        // 	/* supernatural */   'сверхъестественное',
        // 	/* shoujo */         'сёдзё',
        // 	/* shoujoAi */       'сёдзё-ай',
        // 	/* shounen */        'сёнэн',
        // 	/* shounenAi */      'сёнэн-ай',
        // 	/* sports */         'спорт',
        // 	/* seinen */         'сэйнэн',
        // 	/* tragedy */        'трагедия',
        // 	/* thriller */       'триллер',
        // 	/* horror */         'ужасы',
        // 	/* fantasy */        'фэнтези',
        // 	/* school */         'школа',
        // 	/* ecchi */          'этти',
        // 	/* yuri */           'юри',
        // ]
      },
    },
    item: {
      comments: 'Комментарии',
      reviews: 'Рецензии',
      translationDiscussion: 'Обсуждение перевода',
      spoiler: {
        text: 'СПОЙЛЕР',
        show: 'Показать?',
      },
    },
  },
};

// https://youtu.be/oBZ6_PZ9qTA
