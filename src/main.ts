
// reset css styles
import 'normalize.css';

import Vue from 'vue';
Vue.config.productionTip = false;

// web client
import Axios from 'axios';
import VueAxios from 'vue-axios';
Vue.use(VueAxios, Axios.create({
  baseURL: 'http://localhost:3000',
  responseType: 'json',
}));

import App from '@/main.vue';
import router from '@/router';

// client-side storage
import { store } from '@/store';

// site language
import { i18n } from '@/lang';

new Vue({
  i18n,
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
