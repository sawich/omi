import { RouteConfig } from 'vue-router';

export const comicsRoutes: RouteConfig[] = [{
    path: '/comics',
    component: () => import('@/views/comics/comics.vue'),
    children: [ {
      path: '/',
      name: 'comicsHome',
      component: () => import('@/views/comics/entry/home.vue'),
    }, {
      path: 'search',
      name: 'comicsSearch',
      component: () => import('@/views/comics/entry/search.vue'),
    }, {
      path: 'more',
      name: 'comicsMore',
      component: () => import('@/views/comics/entry/more.vue'),
    }],
  }, {
    path: '/comics/item/:id',
    name: 'comicsItem',
    component: () => import('@/views/comics/item.vue'),
  },
];
