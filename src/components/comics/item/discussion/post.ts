
export interface IPostAuthor {
  id: number;
  nick: string;
  name: string;
}

export interface IPost {
  id: number;
  author: IPostAuthor;
  text: string;
  spoiler: boolean;
  likes: number;
  timestamp: string;
}

export interface IRawPost {
  id: number;
  author: {
    id: number
    nick: string
    name: string,
  };
  text: string;
  spoiler: boolean;
  likes: number;
  timestamp: number;
}
